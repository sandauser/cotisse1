<div  ng-app="myApp" ng-controller="about_Controller">

    
    <!DOCTYPE html>
    <html lang="en">
    
    <head>
        <!-- Required meta tags-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="au theme template">
        <meta name="author" content="Hau Nguyen">
        <meta name="keywords" content="au theme template">
    
        <!-- Title Page-->
        <title>Situation</title>
    
        <!-- Fontfaces CSS-->
        <link href="css/font-face.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
        <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
        
        <!-- Bootstrap CSS-->
        <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
        
        <!-- Vendor CSS-->
        <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
        <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
        <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
        <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
        <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
        <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
        
        <!-- Main CSS-->
        <link href="css/theme.css" rel="stylesheet" media="all">
    
    </head>
    
    <body class="animsition">
        <div class="page-wrapper">
           
    
            <!-- MENU SIDEBAR-->
            <aside class="menu-sidebar d-none d-lg-block">
                <div class="logo">
                        <h3 class="pb-2 display-5">Banque En Ligne</h3>
                </div>
                <div class="menu-sidebar__content js-scrollbar1">
                    <nav class="navbar-sidebar">
                        <ul class="list-unstyled navbar__list">
                            
                                <li>
                                        <a href="{{AT}}">
                                            <i class="fas fa-mail-reply-all"></i>Antananarivo vers Tamatave</a>
                                </li>
                                <li>
                                        <a href="{{TA}}">
                                        <i class="fas fa-retweet"></i>Tamatave vers Antananarivo</a>
                                </li>
                                   <li>
                                        <a href="{{AM}}">
                                        <i class="fas fa-mail-forward"></i>Antananarivo vers Majunga</a>
                                </li>
                                <li>
                                        <a href="{{MA}}">
                                        <i class="fas fa-table"></i>Majunga vers Antananarivo</a>
                                </li>
                        </ul>
                    </nav>
                </div>
            </aside>
            <!-- END MENU SIDEBAR-->
    
            <!-- PAGE CONTAINER-->
            <div class="page-container">
                <!-- HEADER DESKTOP-->
                <header class="header-desktop">
                    <div class="section__content section__content--p30">
                        <div class="container-fluid">
                            <div class="header-wrap">
                                
                                    <h2 class="pb-2 display-5"> Situation Personnelle</h2>
                            </div>
                        </div>
                    </div>
                </header>
                <!-- END HEADER DESKTOP-->
    
                <!-- MAIN CONTENT-->
                <div class="main-content">
                    <div class="section__content section__content--p30">
                        <div class="container-fluid">
                          
                               
                            
                            <div class="row m-t-30">
                                    <div class="col-md-12">
                                        <!-- DATA TABLE-->
                                        <div class="table-responsive m-b-40">
                                            <h2>Antananarivo - Tamatave {{p}}</h2>
                                            <br>
                                            <table class="table table-borderless table-data3">
                                                <thead>
                                                    <tr>
                                                        <th> Place occupé </th>
                                                        <th>Place vide</th>
                                                        <th>Depart</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                        <tr>
                                                                <td>{{occupe}}</td>
                                                                <td>{{nonoccupe}}</td>
                                                                <td><input type="button"  ng-click="partirTA()" value="Depart"class="au-btn au-btn--block au-btn--blue m-b-20" /></td>
                                                        </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- END DATA TABLE-->
                                    </div>
                                </div>
                                                   
                                <div class="row m-t-30">
                                    <div class="col-md-12">
                                        <!-- DATA TABLE-->
                                        <div class="table-responsive m-b-40">
                                                <h2> Statistiques </h2>
                                                <br>
                                            <table class="table table-borderless table-data3">
                                                <thead>
                                                    <tr>
                                                        <th> Nombre voiture</th>
                                                        <th> Nombre passager</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                        <tr>
                                                                <td>{{nbVoiAT}}</td>
                                                                <td>{{nbPassAT}}</td>
                                                              </tr>
                                                  
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- END DATA TABLE-->
                                    </div>
                                </div>
                        
                                <div class="row">
                                        <div class="col col-lg-12">
                                          <section class="card">
                                            <div class="card-body text-secondary">Solde Actuelle : {{nm}} </div>
                                          </section>
                                        </div>
                                      </div>
                    </section>
                  </div>
                </div>
                    
                    
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="copyright">
                                        <p>Copyright © 2020 Sanda. All rights reserved</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>
    
        <!-- Jquery JS-->
        <script src="vendor/jquery-3.2.1.min.js"></script>
        <!-- Bootstrap JS-->
        <script src="vendor/bootstrap-4.1/popper.min.js"></script>
        <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
        <!-- Vendor JS       -->
        <script src="vendor/slick/slick.min.js">
        </script>
        <script src="vendor/wow/wow.min.js"></script>
        <script src="vendor/animsition/animsition.min.js"></script>
        <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
        </script>
        <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
        <script src="vendor/counter-up/jquery.counterup.min.js">
        </script>
        <script src="vendor/circle-progress/circle-progress.min.js"></script>
        <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
        <script src="vendor/chartjs/Chart.bundle.min.js"></script>
        <script src="vendor/select2/select2.min.js">
        </script>
    
        <!-- Main JS-->
        <script src="js/main.js"></script>
    
    </body>
    
    </html>
    <!-- end document-->
    

</div>