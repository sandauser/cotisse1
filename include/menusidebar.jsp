 <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                    <h3 class="pb-2 display-5">Gestion de stocks</h3>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                           <li>
                            <a href="configuration.jsp">
                                <i class="fas fa-gears"></i>Configuration</a>
                        </li>
                           <li>
                            <a href="MouvementRedirection">
                                <i class="fas fa-retweet"></i>Mouvement</a>
                        </li>
                        <li>
                            <a href="ListeReportServlet">
                                <i class="fas fa-check"></i>Voir Report</a>
                        </li>
<!--                        <li>
                            <a href="MouvementRedirection">
                                <i class="fas fa-retweet"></i>Mouvements</a>
                        </li>-->
                        <li>
                            <a href="HistoriqueServlet">
                                <i class="fas fa-table"></i>Historique</a>
                        </li>
                    <li>
                            <a href="HistoriqueClient">
                                <i class="fas fa-calendar"></i>HistoriqueUtilisateur</a>
                        </li>
                        <li>
                            <a href="EquivalenceServlet">
                                <i class="fas fa-sitemap"></i>Equivalence</a>
                        </li>
                        
                         <li>
                            <a href="ConsoliderRedirection">
                                <i class="fas fa-retweet"></i>Consolidation</a>
                        </li>
                         <li>
                            <a href="EtatServlet">
                                <i class="fas fa-archive"></i>Etat De Stock</a>
                        </li>
                          <li>
                            <a href="EtatReportServlet">
                                <i class="fas fa-archive"></i>Etat Stock Report</a>
                        </li>
                         <li>
                            <a href="reportServlet">
                                <i class="fas fa-upload"></i>Report</a>
                        </li>
                        <li>
                            <a href="verifReportServlet">
                                <i class="fas fa-retweet"></i>Verification Report</a>
                        </li>
                          <li>
                            <a href="reportnew.jsp">
                                <i class="fas fa-university"></i>ReportNew</a>
                        </li>
                         <li>
                            <a href="cloture.jsp">
                                <i class="fas fa-archive"></i>Cloture</a>
                        </li>
                      
                    </ul>
                </nav>
            </div>
        </aside>