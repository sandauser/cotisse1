var app = angular.module('myApp', ['ngRoute']);
app.config(function ($routeProvider) {
    $routeProvider.when('/login', {templateUrl : 'login.php', controller :'loginController'})
    .when('/ant_tam/:idclient', {templateUrl : 'ant_tam.php', controller :'ant_tam_Controller'})
    .when('/tam_ant/:idclient', {templateUrl : 'tam_ant.php', controller :'tam_ant_Controller'})
    .when('/ant_ma/:idclient', {templateUrl : 'ant_ma.php', controller :'ant_ma_Controller'})
    .when('/ma_ant/:idclient', {templateUrl : 'ma_ant.php', controller :'ma_ant_Controller'})
	.when('/inscription', {templateUrl : 'inscription.php', controller :'inscription_Controller'})
	.when('/about', {templateUrl : 'about.php', controller :'about_Controller'})
   
});
app.controller("indexController", function($scope,$http) { 
    }); 
app.controller("about_Controller", function($scope,$http,$routeParams,$window) {  
	$scope.listeAT;
	$scope.occupe=0;
	$scope.nonoccupe=0;
	//maka place occuppe
    $http.get("https://essaivao.herokuapp.com/listePlace?desti=AT")
		.then(function (response) {
				$scope.listeAT=response.data;	
				var i=0;
				for(i=0; i<$scope.listeAT.length; i++){
					if($scope.listeAT[i]["dispo"]=="1"){
						$scope.occupe=$scope.occupe+1;
					}
				}
				$scope.nonoccupe=18-$scope.occupe;
	});
	
	//depart
	$scope.partirTA=function(){
		$http.get("https://essaivao.herokuapp.com/saveNombre?desti=AT&nbclient="+$scope.occupe)
		.then(function (response) {	
		});
		
		//nombre vo package
		$scope.nbVoiAT=$scope.nbVoiAT+1;
		$scope.nbPassAT=$scope.nbPassAT+$scope.occupe;
		
		//reinitialisation
		var m=0;
		for(m=1; m<=18; m++){//at 1-18      ta 19 - 
			$http.get("https://essaivao.herokuapp.com/reinitial?id="+m)
			.then(function (response) {	
			});
		}
		
		$scope.occupe=0;
		$scope.nonoccupe=0;
		
					
	}
	
	
	//nombre voiture lasa de passager
	$scope.nbVoiAT=0;
	$scope.nbPassAT=0;
	$http.get("https://essaivao.herokuapp.com/listeNombre?desti=AT")
		.then(function (response) {
				$scope.tmp=response.data;	
				var i=0;
				for(i=0; i<$scope.tmp.length; i++){
					$scope.nbVoiAT=$scope.nbVoiAT+1;
					$scope.nbPassAT=$scope.nbPassAT+parseInt($scope.tmp[i]["nombre_clien"]);
					
				}
	});
	
	
	
	
	$scope.listeTA;
    $http.get("https://essaivao.herokuapp.com/listePlace?desti=TA")
		.then(function (response) {
				$scope.listeTA=response.data;	
	});
	
	$scope.listeAM;
    $http.get("https://essaivao.herokuapp.com/listePlace?desti=AM")
		.then(function (response) {
				$scope.listeAM=response.data;	
	});
	$scope.listeMA;
    $http.get("https://essaivao.herokuapp.com/listePlace?desti=MA")
		.then(function (response) {
				$scope.listeMA=response.data;	
	});
	$scope.p="ok";
	
	
});  




app.controller("loginController", function($scope,$http,$routeParams,$window) {  
    $scope.login = function(){
        console.log("Le nom est "+$scope.anarana+" et le Mot De Passe "+$scope.pass);
        $http.get("https://essaivao.herokuapp.com/login?nom="+$scope.anarana+"&mdp="+$scope.pass)
			.then(function (response) {
			//	$scope.names = response.data;
				if(response.data[0]["statut"]=="failled"){
					$scope.erreurR="Login ou mot de passe erreur!";
				}
				if(response.data[0]["statut"]=="succes"){
					var idclient=response.data[0]["idclient"];
					
					 $window.location.href = '#!ant_tam/'+idclient;
				}
				
			});
			//var idclient=3;
		// $window.location.href = '#!ant_tam/'+idclient;
    };  
  
});  
 app.controller("inscription_Controller", function($scope,$http,$routeParams,$window) {  
    
	$scope.idclient="";
	$scope.nom="";
	$scope.mot_de_passe="";
	$scope.solde="";
	$scope.erreur="";
	$scope.inscrire = function(){
		 $http.get("https://essaivao.herokuapp.com/saveClient?nom="+$scope.nom+"&mdp="+$scope.mot_de_passe+"&sold="+$scope.solde)
		.then(function (response) {
			alert("Inscription reussite!!!!");
			
		});
		$window.location.href = '#!login/';
		
	};  
	//$window.location.href = '#!login/';
    
  
});  
 
app.controller("ant_tam_Controller", function($scope,$http,$routeParams,$window) {
	$scope.liste;
//manao liste place	
	$http.get("https://essaivao.herokuapp.com/listePlace?desti=AT")
		.then(function (response) {
				$scope.liste=response.data;
				
				var j=0;
				for(j=0; j<$scope.liste.length; j++){
					if($scope.liste[j]["dispo"]=="1" && $scope.liste[j]["idclient"]==$scope.idclient){
						$scope.total=$scope.total+$scope.prix;
					}
				}
				
	
				
					
		});
    $scope.idclient = $routeParams.idclient;
	
	
	//maka client de manao bienvenu
	 $scope.anarana="";
	  $scope.solde=0;
	 $http.get("https://essaivao.herokuapp.com/findclientbyid?id="+$scope.idclient)
		.then(function (response) {
				$scope.anarana=response.data["nom"];
				$scope.solde=response.data["solde"];
		});
	
		
	
	//lien menu
    $scope.AT = "#!ant_tam/"+$routeParams.idclient;
    $scope.TA = "#!tam_ant/"+$routeParams.idclient;
    $scope.AM = "#!ant_ma/"+$routeParams.idclient;
    $scope.MA = "#!ma_ant/"+$routeParams.idclient;
	
	//action
	$scope.prix=20000;
	$scope.nombre_reserver=0;
	$scope.total=0;

	
	
	
	
	$scope.reserver = function(idplace){
		var indice=0;
		var r=0;
		for(r=0; r<$scope.liste.length; r++){
			if($scope.liste[r]["idplace"]==idplace){
				indice=r;
			}
		}
		var anciencolor= document.getElementById("subRepeat"+idplace).style.color;
		//tsy reserver de las reserver par moi (client miconnect)
	  if(anciencolor=="white"){
		  document.getElementById("subRepeat"+idplace).style.color="green";
		  $scope.nombre_reserver=$scope.nombre_reserver+1;
		  $scope.total=$scope.total+$scope.prix;
			var indiceListe=indice;
			$scope.liste[indiceListe]["idclient"]=$scope.idclient;
			$scope.liste[indiceListe]["dispo"]="2";
			// alert($scope.liste[indiceListe]["dispo"]);
		  //las occuper
		//  changerajaxoccuper(idbouttoncolor,'1');
	  }
	  if(anciencolor=="green"){
		  document.getElementById("subRepeat"+idplace).style.color="white";
		  $scope.nombre_reserver=$scope.nombre_reserver-1;
		  $scope.total=$scope.total-$scope.prix;
		  var indiceListe=indice;
		  $scope.liste[indiceListe]["dispo"]="0";
		  $scope.liste[indiceListe]["idclient"]="";
		 // alert($scope.liste[indiceListe]["dispo"]);
		 // changerajaxoccuper(idbouttoncolor,'0');
	  }
	  
	}
	$scope.ok="";
	//place selectionnner
	$scope.valider=function(idplace){
		$scope.val=$scope.solde-$scope.total;
		if($scope.val>=0){
			var i=0;
			for(i=0; i<$scope.liste.length; i++){
				var url="";
				if($scope.liste[i]["dispo"]==2)$scope.liste[i]["dispo"]=1;
					url="https://essaivao.herokuapp.com/updatereserver?id="+$scope.liste[i]["idplace"]+"&idclient="+$scope.idclient+"&dispo="+$scope.liste[i]["dispo"];
				if($scope.liste[i]["dispo"]==0)
					url="https://essaivao.herokuapp.com/update0?id="+$scope.liste[i]["idplace"];
				$http.get(url)
				.then(function (response) {
				});
				
			}
			$http.get("https://essaivao.herokuapp.com/updateSolde?id="+$scope.idclient+"&sold="+$scope.val)
			.then(function (response) {
						
			});
			$scope.ok="Place reserver";
			$scope.solde=$scope.val;
		}
		else{
			$scope.ok="Attention! Solde est insuffisant";
		}
		
	}
	
	//place hasard
	$scope.valideHasard=function(idplace){
		var is=parseInt($scope.isa);
		$scope.val=$scope.solde-$scope.total;
		if($scope.val>=0){
			var j=0;
			var count=0;
			for(j=0; j<$scope.liste.length; j++){
				
					if($scope.liste[j]["dispo"]=="0"){
						if(count<is){
							$scope.liste[j]["dispo"]="2";
							count=count+1;
						}
					}
					
			}
			var i=0;
			for(i=0; i<$scope.liste.length; i++){
				var url="";
				if($scope.liste[i]["dispo"]==2){
					$scope.liste[i]["dispo"]=1;
					url="https://essaivao.herokuapp.com/updatereserver?id="+$scope.liste[i]["idplace"]+"&idclient="+$scope.idclient+"&dispo="+$scope.liste[i]["dispo"];
				}
				if($scope.liste[i]["dispo"]==0){
					url="https://essaivao.herokuapp.com/update0?id="+$scope.liste[i]["idplace"];
				}
				else { url="https://essaivao.herokuapp.com/updatereserver?id="+$scope.liste[i]["idplace"]+"&idclient="+$scope.liste[i]["idclient"]+"&dispo="+$scope.liste[i]["dispo"];
				}
				$http.get(url)
				.then(function (response) {
				});
				
			}
			$http.get("https://essaivao.herokuapp.com/updateSolde?id="+$scope.idclient+"&sold="+$scope.val)
			.then(function (response) {
						
			});
			$scope.ok="Place reserver";
			$scope.solde=$scope.val;
			
			
			
		}
		else{
			$scope.ok="Attention! Solde est insuffisant";
		}
		
	}

});

app.controller("tam_ant_Controller", function($scope,$http,$routeParams,$window) {  
      $scope.idclient = $routeParams.idclient;
	
    $scope.AT = "#!ant_tam/"+$routeParams.idclient;
    $scope.TA = "#!tam_ant/"+$routeParams.idclient;
    $scope.AM = "#!ant_ma/"+$routeParams.idclient;
    $scope.MA = "#!ma_ant/"+$routeParams.idclient;
	
	$scope.prix="20 000 Ariary";
	$scope.nombre_reserver=0;
	$scope.total=0;
	$scope.reserver = function(place){
		var anciencolor= document.getElementById(place).style.color;
	  if(anciencolor=="white"){
		  document.getElementById(place).style.color="green";
		  $scope.nombre_reserver=$scope.nombre_reserver+1;
		  $scope.total=$scope.total+20000;
		
		  //las occuper
		//  changerajaxoccuper(idbouttoncolor,'1');
	  }
	  if(anciencolor=="green"){
		  document.getElementById(place).style.color="white";
		  $scope.nombre_reserver=$scope.nombre_reserver-1;
		  $scope.total=$scope.total-20000;
		  
		 // changerajaxoccuper(idbouttoncolor,'0');
	  }
	  
	}
      
}); 

app.controller("ant_ma_Controller", function($scope,$http,$routeParams,$window) {  
    $scope.idclient = $routeParams.idclient;
	
    $scope.AT = "#!ant_tam/"+$routeParams.idclient;
    $scope.TA = "#!tam_ant/"+$routeParams.idclient;
    $scope.AM = "#!ant_ma/"+$routeParams.idclient;
    $scope.MA = "#!ma_ant/"+$routeParams.idclient;

		
	$scope.prix="20 000 Ariary";
	$scope.nombre_reserver=0;
	$scope.total=0;
	$scope.reserver = function(place){
		var anciencolor= document.getElementById(place).style.color;
	  if(anciencolor=="white"){
		  document.getElementById(place).style.color="green";
		  $scope.nombre_reserver=$scope.nombre_reserver+1;
		  $scope.total=$scope.total+20000;
		
		  //las occuper
		//  changerajaxoccuper(idbouttoncolor,'1');
	  }
	  if(anciencolor=="green"){
		  document.getElementById(place).style.color="white";
		  $scope.nombre_reserver=$scope.nombre_reserver-1;
		  $scope.total=$scope.total-20000;
		  
		 // changerajaxoccuper(idbouttoncolor,'0');
	  }
	  
	}
});

app.controller("ma_ant_Controller", function($scope,$http,$routeParams,$window) { 
          $scope.idclient = $routeParams.idclient;
	
    $scope.AT = "#!ant_tam/"+$routeParams.idclient;
    $scope.TA = "#!tam_ant/"+$routeParams.idclient;
    $scope.AM = "#!ant_ma/"+$routeParams.idclient;
    $scope.MA = "#!ma_ant/"+$routeParams.idclient;
	
	$scope.prix="20 000 Ariary";
	$scope.nombre_reserver=0;
	$scope.total=0;
	$scope.reserver = function(place){
		var anciencolor= document.getElementById(place).style.color;
	  if(anciencolor=="white"){
		  document.getElementById(place).style.color="green";
		  $scope.nombre_reserver=$scope.nombre_reserver+1;
		  $scope.total=$scope.total+20000;
		
		  //las occuper
		//  changerajaxoccuper(idbouttoncolor,'1');
	  }
	  if(anciencolor=="green"){
		  document.getElementById(place).style.color="white";
		  $scope.nombre_reserver=$scope.nombre_reserver-1;
		  $scope.total=$scope.total-20000;
		  
		 // changerajaxoccuper(idbouttoncolor,'0');
	  }
	  
	}
	
});  			
			