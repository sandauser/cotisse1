<div  ng-app="myApp" ng-controller="ant_ma_Controller">

    
        <!DOCTYPE html>
        <html lang="en">
        
        <head>
            <!-- Required meta tags-->
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="description" content="au theme template">
            <meta name="author" content="Hau Nguyen">
            <meta name="keywords" content="au theme template">
        
            <!-- Title Page-->
            <title>Situation</title>
        
            <!-- Fontfaces CSS-->
            <link href="css/font-face.css" rel="stylesheet" media="all">
            <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
            <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
            <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
            
            <!-- Bootstrap CSS-->
            <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
            
            <!-- Vendor CSS-->
            <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
            <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
            <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
            <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
            <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
            <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
            <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
            
            <!-- Main CSS-->
            <link href="css/theme.css" rel="stylesheet" media="all">
        
        </head>
        
        <body class="animsition">
            <div class="page-wrapper">
               
        
                <!-- MENU SIDEBAR-->
                <aside class="menu-sidebar d-none d-lg-block">
                    <div class="logo">
                            <h3 class="pb-2 display-5"> Reservation</h3>
                    </div>
                    <div class="menu-sidebar__content js-scrollbar1">
                        <nav class="navbar-sidebar">
                            <ul class="list-unstyled navbar__list">
                                
                                    <li>
                                        <a href="{{AT}}">
                                            <i class="fas fa-mail-reply-all"></i>Antananarivo vers Tamatave</a>
                                </li>
                                <li>
                                        <a href="{{TA}}">
                                        <i class="fas fa-retweet"></i>Tamatave vers Antananarivo</a>
                                </li>
                                   <li>
                                        <a href="{{AM}}">
                                        <i class="fas fa-mail-forward"></i>Antananarivo vers Majunga</a>
                                </li>
                                <li>
                                        <a href="{{MA}}">
                                        <i class="fas fa-table"></i>Majunga vers Antananarivo</a>
                                </li>
                           
                            </ul>
                        </nav>
                    </div>
                </aside>
                <!-- END MENU SIDEBAR-->
        
                <!-- PAGE CONTAINER-->
                <div class="page-container">
                    <!-- HEADER DESKTOP-->
                    <header class="header-desktop">
                        <div class="section__content section__content--p30">
                            <div class="container-fluid">
                                <div class="header-wrap">
                                    
                                        <h2 class="pb-2 display-5"> COTISSE-TRANSPORT-MADAGASCAR</h2>
										<a href="#!login/"><button class="au-btn au-btn--block au-btn--green m-b-20" >Déconnexion</button></a>
                                </div>
                            </div>
                        </div>
                    </header>
                    <!-- END HEADER DESKTOP-->
        
                    <!-- MAIN CONTENT-->
                    <div class="main-content">
                        <div class="section__content section__content--p30">
                            <div class="container-fluid">
							
							
							
								 <div class="row">
                                            <div class="col-lg-12">
                                                <div class="card">
                                                    <div class="card-header">Antananarivo - Majunga </div>
                                                    <div class="card-body">
                                                        <div class="card-title">
                                                            <h3 class="text-center title-2"> Emplacement</h3>
                                                        </div>
                                                        <hr>
                                                        <form>
                                                            
                                                            <div>
																<table style="margin: 0 0 0 36%">
																	<tr>
																		<td style="width:70px; height:70px;"><button id="a1" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;" ng-click="reserver('a1')">A1</button></td>
																		<td style="width:70px;"><button id="a2" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('a2')">A2</button></td>
																		<td style="width:70px;"><button id="a3" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('a3')">A3</button></td>
																		<td style="width:70px;"><button id="a4" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('a4')">A4</button></td>
																	</tr>
																	<tr>
																		<td style="width:70px; height:70px;"><button id="b1" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('b1')">B1</button></td>
																		<td style="width:70px;"><button id="b2" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('b2')">B2</button></td>
																		<td style="width:70px;"><button id="b3" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('b3')">B3</button></td>
																		<td style="width:70px;"><button id="b4" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('b4')">B4</button></td>
																	</tr>
																	<tr>
																		<td style="width:70px; height:70px;"><button id="c1" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('c1')">C1</button></td>
																		<td style="width:70px;"><button id="c2" class="btn btn-lg btn-info btn-block" style="width:50px; color:red;"  ng-click="reserver('c2')">C2</button></td>
																		<td style="width:70px;"><button id="c3" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('c3')">C3</button></td>
																		<td style="width:70px;"><button id="c4" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('c4')">C4</button></td>
																	</tr>
																	<tr>
																		<td style="width:70px; height:70px;"><button id="d1" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('d1')">D1</button></td>
																		<td style="width:70px;"><button id="d2" class="btn btn-lg btn-info btn-block" style="width:50px; color:red;"  ng-click="reserver('d2')">D2</button></td>
																		<td style="width:70px;"><button id="d3" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('d3')">D3</button></td>
																		<td style="width:70px;"><button id="d4" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('d4')">D4</button></td>
																	</tr>
																	<tr>
																		<td style="width:70px; height:70px;"><button id="e1" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('e1')">E1</button></td>
																		<td style="width:70px;"><button id="e2" class="btn btn-lg btn-info btn-block" style="width:50px; color:white;"  ng-click="reserver('e2')">E2</button></td>
																		
																	</tr>
																</table>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										
							
							
                                    <div class="row">
                                            <div class="col-lg-12">
                                                <div class="card">
                                                   
                                                    <div class="card-body">
                                                        <div class="card-title">
                                                            <h3 class="text-center title-2"> Reservation non payer </h3>
                                                        </div>
                                                        <hr>
                                                        <form>
                                                            
                                                           <div class="form-group has-success">
                                                                    <label for="cc-name" class="control-label mb-1">Prix : </label> &nbsp {{prix}}                                                                    
                                                            </div>
                                                               <div class="form-group has-success">
                                                                    <label for="cc-name" class="control-label mb-1">Nombre reserver : </label> &nbsp {{nombre_reserver}}
                                                                    
                                                            </div>
															
															<div class="form-group has-success">
                                                                    <label for="cc-name" class="control-label mb-1">Total : </label> &nbsp {{total}} Ariary
                                                                    
                                                            </div>
                                                          
                                                            
                                                            <div>
                                                                <button id="payment-button" ng-click="retraitFunc()" type="submit" class="btn btn-lg btn-info btn-block">
                                                                    <i class="fa fa-check fa-lg"></i>&nbsp;
                                                                    <span id="payment-button-amount">Valider</span>
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                   
								   
								   
								   
								   
								   
								   
								   
								   
								   
								   
				 <div class="section__content section__content--p30">
                        <div class="container-fluid">
                          
                               
                            
                            <div class="row m-t-30">
                                    <div class="col-md-12">
                                        <!-- DATA TABLE-->
                                        <div class="table-responsive m-b-40">
                                            <h2> Liste des reservations</h2>
                                            <br>
                                            <table class="table table-borderless table-data3">
                                                <thead>
                                                    <tr>
                                                        <th> Place </th>
                                                        <th>Montant</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                        <tr ng-repeat="value in values ">
                                                                <td>Client {{n}}</td>
                                                                <td>{{value.type}}</td>
                                                                <td>{{value.valeur}}</td>
                                                        </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- END DATA TABLE-->
                                    </div>
                                </div>
                                                   
               
                        
                                <div class="row">
                                        <div class="col col-lg-12">
                                          <section class="card">
                                            <div class="card-body text-secondary">Solde Actuelle : {{nm}} </div>
                                          </section>
                                        </div>
                                      </div>
                    </section>
                  </div>
                </div>
								  
								  
								  
								  
								  
								  
								  
								  
                                
                        
                        
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="copyright">
                                            <p>Copyright © 2020 Sanda. All rights reserved</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        
            </div>
        
            <!-- Jquery JS-->
            <script src="vendor/jquery-3.2.1.min.js"></script>
            <!-- Bootstrap JS-->
            <script src="vendor/bootstrap-4.1/popper.min.js"></script>
            <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
            <!-- Vendor JS       -->
            <script src="vendor/slick/slick.min.js">
            </script>
            <script src="vendor/wow/wow.min.js"></script>
            <script src="vendor/animsition/animsition.min.js"></script>
            <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
            </script>
            <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
            <script src="vendor/counter-up/jquery.counterup.min.js">
            </script>
            <script src="vendor/circle-progress/circle-progress.min.js"></script>
            <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
            <script src="vendor/chartjs/Chart.bundle.min.js"></script>
            <script src="vendor/select2/select2.min.js">
            </script>
        
            <!-- Main JS-->
            <script src="js/main.js"></script>
        
        </body>
        
        </html>
    
    </div>